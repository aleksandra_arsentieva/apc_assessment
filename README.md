## Required software
* Java JDK 8+
* Maven installed and in your classpath

## How to execute the tests
After cloning this project:

1. Navigate to the project folder using the Terminal / Command prompt
2. Execute the following: `mvn clean verify`
3. Wait until process finished
4. Open a report at `target/site/serenity/index.html` folder
