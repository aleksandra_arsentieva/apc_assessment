package testcases;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.given;
import static utils.CommonAssertions.*;
import static utils.Requests.*;

@RunWith(SerenityRunner.class)
public class RestApiTest {

    public static String ERROR_POSTCODE = "OX49";
    public static String OK_POSTCODE = "OX49 5NU";
    public static String OK_OUTCODE = "OX49";
    public static String OK_INCODE = "5NU";
    public static String OK_POSTCODE_2 = "IG3 8HR";
    public static String OK_POSTCODE_COUNTRY = "England";

    @Before
    public void setup() {
        RestAssured.baseURI = "https://api.postcodes.io";
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

    @Test
    public void getErrorPostcodeTest() {
        Response response = getUrl("postcodes/" + ERROR_POSTCODE);
        checkResponse404(response);
        checkResponseWithArgInBody(response, "error", "Invalid postcode");
    }

    @Test
    public void getEmptyPostcodeTest() {
        Response response = getUrl("postcodes/");
        checkResponse400(response);
        checkResponseWithArgInBody(response, "error", "No postcode query submitted. Remember to include query parameter");
    }

    @Test
    public void getPostcodeTest() {
        Response response = getUrl("postcodes/" + OK_POSTCODE);
        checkResponse200(response);
        checkResponseWithBody(response, "schemas/get_postcode.json");
        checkResponseWithArgInBody(response, "result.postcode", OK_POSTCODE);
        checkResponseWithArgInBody(response, "result.country", OK_POSTCODE_COUNTRY);
        checkResponseWithArgInBody(response, "result.outcode", OK_OUTCODE);
        checkResponseWithArgInBody(response, "result.incode", OK_INCODE);
        checkResponseWithArgInBody(response, "result.quality", 1);
    }

    @Test
    public void postOnePostcodeTest() {
        String[] postcodes = new String[]{OK_POSTCODE};
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse200(response);
        checkResponseWithBody(response, "schemas/post_one_postcode.json");
        checkResponseWithArgInBody(response, "result[0].query", OK_POSTCODE);
        checkResponseWithArgInBody(response, "result[0].result.postcode", OK_POSTCODE);
        checkResponseWithArgInBody(response, "result[0].result.country", OK_POSTCODE_COUNTRY);
        checkResponseWithArgInBody(response, "result[0].result.outcode", OK_OUTCODE);
        checkResponseWithArgInBody(response, "result[0].result.incode", OK_INCODE);
        checkResponseWithArgInBody(response, "result[0].result.quality", 1);
    }

    @Test
    public void postTwoOkPostcodeTest() {
        String[] postcodes = new String[]{OK_POSTCODE, OK_POSTCODE_2};
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse200(response);
        checkResponseWithBody(response, "schemas/post_two_postcode.json");
        checkResponseWithArgInBody(response, "result[0].query", OK_POSTCODE);
        checkResponseWithArgInBody(response, "result[1].query", OK_POSTCODE_2);
        checkResponseWithArgInBody(response, "result.result[0].postcode", OK_POSTCODE);
        checkResponseWithArgInBody(response, "result.result[1].postcode", OK_POSTCODE_2);
    }

    @Test
    public void postOneGoodOkPostcodeTest() {
        String[] postcodes = new String[]{OK_POSTCODE, ERROR_POSTCODE};
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse200(response);
        checkResponseWithBody(response, "schemas/post_two_with_one_error.json");
        checkResponseWithArgInBody(response, "result[0].query", OK_POSTCODE);
        checkResponseWithArgInBody(response, "result[1].query", ERROR_POSTCODE);
        checkResponseWithArgInBody(response, "result[1].result", null);
    }

    @Test
    public void postWithErrorPostcodeTest() {
        String[] postcodes = new String[]{ERROR_POSTCODE};
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse200(response);
        checkResponseWithArgInBody(response, "result[0].query", ERROR_POSTCODE);
        checkResponseWithArgInBody(response, "result[0].result", null);
    }

    @Test
    public void postWithBadFormatPostcodeTest() {
        String postcodes = OK_POSTCODE;
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse400(response);
        checkResponseWithArgInBody(response, "error", "Invalid data submitted. You need to provide a JSON array");
    }

    @Test
    public void postWithoutPostcodeTest() {
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", (Object) null);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse400(response);
        checkResponseWithArgInBody(response, "error", "Invalid JSON query submitted. \nYou need to submit a JSON object with an array of postcodes or geolocation objects.\nAlso ensure that Content-Type is set to application/json\n");
    }

    @Test
    public void postWithoutContentTypeTest() {
        String[] postcodes = new String[]{OK_POSTCODE};
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrlWithoutContentType("/postcodes", requestBody);
        checkResponse400(response);
        checkResponseWithArgInBody(response, "error", "Invalid JSON query submitted. \nYou need to submit a JSON object with an array of postcodes or geolocation objects.\nAlso ensure that Content-Type is set to application/json\n");
    }

    @Test
    public void postTooManyPostCodesTest() {
        String[] postcodes = new String[101];
        for (int i=0; i<postcodes.length; i++) {
            postcodes[i] = OK_POSTCODE;
        }
        JSONObject requestBody = new JSONObject();
        requestBody.put("postcodes", postcodes);
        Response response = postUrl("/postcodes", requestBody);
        checkResponse400(response);
        checkResponseWithArgInBody(response, "error", "Too many postcodes submitted. Up to 100 postcodes can be bulk requested at a time");
    }
}
