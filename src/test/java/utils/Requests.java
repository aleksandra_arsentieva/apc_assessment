package utils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;

public class Requests {
    public static Response getUrl(String url) {
        return RestAssured.when().get(url);
    }

    public static Response postUrl(String url, JSONObject requestBody) {
        RequestSpecification request = given().accept(ContentType.JSON).contentType(ContentType.JSON);
        request.body(requestBody.toString());
        return request.post(url);
    }

    public static Response postUrlWithoutContentType(String url, JSONObject requestBody) {
        RequestSpecification request = given();
        request.body(requestBody.toString());
        return request.post(url);
    }
}
