package utils;

import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import io.restassured.response.Response;

import java.util.logging.Level;
import java.util.logging.Logger;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.is;

public class CommonAssertions {
    public static void checkResponse200(Response response) {
        response.then().assertThat().statusCode(200);
    }

    public static void checkResponse404(Response response) {
        response.then().assertThat().statusCode(404);
    }

    public static void checkResponse400(Response response) {
        response.then().assertThat().statusCode(400);
    }

    public static void checkResponseWithArgInBody(Response response, String argName, Object argValue) {
        response.then().body(argName, is(argValue));
    }

    public static void checkResponseWithBody(Response response, String path) {
        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.newBuilder()
                .setValidationConfiguration(
                        ValidationConfiguration.newBuilder()
                                .setDefaultVersion(SchemaVersion.DRAFTV4).freeze())
                .freeze();
        response.then().body(matchesJsonSchemaInClasspath(path).using(jsonSchemaFactory));
    }
}
